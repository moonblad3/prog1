#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>

using namespace std;

class Polartransz{
    public:
        Polartransz(){
            Nemtarolt = true;
            srand(time(NULL));
        }
        ~Polartransz(){

        }

        double kov();
    private:
        bool Nemtarolt;
        double tarolt; 
};

double Polartransz::kov(){
    
    if (Nemtarolt){
        double u1,u2,v1,v2,w;
            do{
                u1 = rand() / (RAND_MAX + 1.0);
                u2 = rand() / (RAND_MAX + 1.0);
                v1 = 2*u1-1;
                v2 = 2*u2-1;
                w = v1*v1+v2*v2;
            }
            while (w > 1);

            double r = sqrt ( (-2 * log (w)) / w);

            tarolt = r * v2;
            Nemtarolt = !Nemtarolt;
   
        return r * v1;
    }

    else {
        Nemtarolt = !Nemtarolt;
        return tarolt;
    }
};

int main(){

    Polartransz veletlen;

    for( int i = 0; i<10; ++i){
        cout<<veletlen.kov()<<endl;
    }

}