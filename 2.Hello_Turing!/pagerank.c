#include <stdio.h>
#include <math.h>

void kiir (double segedtomb[], int db)
{
  int i;

  for (i = 0; i < db; ++i)
    printf ("%f\n", segedtomb[i]);
}

double tavolsag (double oldal[], double oldalh[], int n)
{
  double osszeg = 0.0;
  int i;

  for (i = 0; i < n; ++i)
    osszeg += (oldalh[i] - oldal[i]) * (oldalh[i] - oldal[i]);

  return sqrt(osszeg);
}


int main(){

  double tomb[4][4] = {
    {0.0,0.0,1.0/3.0,0.0},
    {1.0,1.0/2.0,1.0/3.0,1.0},
    {0.0,1.0/2.0,0.0,0.0},
    {0.0,0.0,1.0/3.0,0.0}
  };


    double oldal[4] = { 0.0, 0.0, 0.0, 0.0 };
    double oldalh[4] = { 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0 };



  for (;;)
    {

      for (int i = 0; i < 4; ++i)
	{
	  oldal[i] = 0.0;
	  for (int j = 0; j < 4; ++j)
	    oldal[i] += (tomb[i][j] * oldalh[j]);
	}
      if (tavolsag (oldal, oldalh, 4) < 0.00000001)
	break;

      for (int i = 0; i < 4; ++i)
	oldalh[i] = oldal[i];

    }

  kiir (oldal, 4);

  return 0;
}